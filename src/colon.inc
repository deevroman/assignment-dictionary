%define LAST_ELEM 0

%macro colon 2
    %2:
        dq   LAST_ELEM
        %define LAST_ELEM %2
        db   %1, 0
%endmacro