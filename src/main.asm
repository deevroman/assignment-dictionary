%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define PTR_SIZE 8
%define BUFFER_SIZE 256

global _start

section .rodata
buffer_overflow_msg: db `Buffer overflow.\n`, 0
key_not_found_msg: db `Key not found.\n`, 0

section .bss
buffer: resb BUFFER_SIZE


section .text
_start:
    mov   rdi, buffer
    mov   rsi, BUFFER_SIZE
    call  read_word

    test  rax, rax
    je    .buffer_overflow

    mov   rdi, rax
    mov   rsi, LAST_ELEM
    call  find_word

    test  rax, rax
    je    .key_not_found

    add   rax, PTR_SIZE 

    push  rax
    mov   rdi, rax
    call  string_length  
    pop   rdi
    add   rdi, rax      
    inc   rdi             
    call  print_string
    jmp   exit0

    .key_not_found:
        mov   rdi, key_not_found_msg
        call  print_error_string
        mov   rdi, -1
        jmp   exit

    .buffer_overflow:
        mov   rdi, buffer_overflow_msg
        call  print_error_string
        mov   rdi, -1
        jmp   exit
    
