%include "lib.inc"

%define PTR_SIZE 8

global find_word

section .text


; Params:
;   rdi: String link
;   rsi: Dict link
; Out:
;   rax: Key link
;   0 if not found 
find_word:
    push  rdi
    push  rsi
    add   rsi, PTR_SIZE 
    call  string_equals
    pop   rsi
    pop   rdi

    test  rax, rax  
    jne   .found          

    mov   rsi, [rsi]       
    test  rsi, rsi
    jne   find_word

    xor   rax, rax
    ret

    .found:
        mov   rax, rsi
        ret